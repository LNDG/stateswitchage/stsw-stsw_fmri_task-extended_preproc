#!/bin/bash

fun_name="b2_gunzip"
job_name="stsw_gunzip"

rootpath="$(pwd)/.."
rootpath=$(builtin cd $rootpath; pwd)

# path to the text file with all subject ids:
path_ids="${rootpath}/code/id_list_mr.txt"
# read subject ids from the list of the text file
IDS=$(cat ${path_ids} | tr '\n' ' ')

for subj in $IDS; do
for run in $(seq 1 4); do
	echo "#!/bin/bash"                    										> job.slurm
	echo "#SBATCH --job-name ${job_name}_${subj}_${run}" 						>> job.slurm
	echo "#SBATCH --cpus-per-task 4"											>> job.slurm
	echo "#SBATCH --mem 4gb" 													>> job.slurm
	echo "#SBATCH --time 00:10:00" 												>> job.slurm
	echo "#SBATCH --output ${rootpath}/log/${job_name}_${subj}_${run}.out"		>> job.slurm
	echo "#SBATCH --workdir ." 													>> job.slurm
	echo "gunzip ${rootpath}/data/data_clean/${subj}_run-${run}_regressed.nii.gz" 	>> job.slurm
	sbatch job.slurm
	rm -f job.slurm
done
done
