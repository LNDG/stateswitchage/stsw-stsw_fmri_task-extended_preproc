function c2_common_coords_summary()
% Identify common GM coordinates to constrain analyses to.
%   nii_coords                    | all voxels
%   final_coords                  | non-NaN, non-zero-power GM voxels across subjects

% PLS requires exlusively non-NaN voxels. GM voxels and non-Zero-power is
% optional, however may be recommended. Note that by focussing on
% non-Zero-power voxels, each subject contributes an identical amount of
% samples (N) to the analysis. Non-Zero-power voxels are usually located
% outside the brain and may inter-individually differ in number depending 
% on the coregistration with MNI. 

%% paths & setup

    currentFile = mfilename('fullpath');
    [pathstr,~,~] = fileparts(currentFile);
    cd(fullfile(pathstr,'..'))
    rootpath = pwd;

    pn.standards	= fullfile(rootpath, 'data', 'standards');
    pn.voxeloverlap = fullfile(rootpath, 'data', 'voxeloverlap');
    
    addpath(genpath(fullfile(rootpath, 'tools', 'nifti_toolbox'))); % NIFTI_toolbox

    % N = 44 YA + 53 OA;
    IDs = {'1117';'1118';'1120';'1124';'1125';'1126';'1131';'1132';'1135';'1136';...
        '1151';'1160';'1164';'1167';'1169';'1172';'1173';'1178';'1182';'1214';'1215';...
        '1216';'1219';'1223';'1227';'1228';'1233';'1234';'1237';'1239';'1240';'1243';...
        '1245';'1247';'1250';'1252';'1257';'1261';'1265';'1266';'1268';'1270';'1276';'1281';...
        '2104';'2107';'2108';'2112';'2118';'2120';'2121';'2123';'2125';'2129';'2130';...
        '2132';'2133';'2134';'2135';'2139';'2140';'2145';'2147';'2149';'2157';...
        '2160';'2201';'2202';'2203';'2205';'2206';'2209';'2210';'2211';'2213';'2214';...
        '2215';'2216';'2217';'2219';'2222';'2224';'2226';'2227';'2236';'2238';...
        '2241';'2244';'2246';'2248';'2250';'2251';'2252';'2258';'2261'};
    
    RunID={'1'; '2'; '3'; '4'};
    
%% get grey matter coordinates based on MNI GM mask

    fname_struct = fullfile(pn.standards, 'data', ...
        'mni_icbm152_nlin_sym_09c', 'mni_icbm152_gm_tal_nlin_sym_09c_MNI_3mm.nii.gz');

    GM_coords = load_nii(fname_struct);
    GM_coords_nz = reshape(GM_coords.img, [],1);
    GM_coords_nn = GM_coords_nz;
    
%% identify non-NaN voxels & non-zero power voxels

    nii_coords=1:64*76*64 ; % 3mm

    for indID = 1:numel(IDs)
        for indRun = 1:numel(RunID)
            try
                % non-zero
                coords=load_nii(fullfile(pn.voxeloverlap, ...
                    ['subcoords_nz_',IDs{indID},'_r', RunID{indRun},'.nii']));
                coords=reshape(coords.img,[],1);
                GM_coords_nz = cat(2, GM_coords_nz, coords);
                % non-NaN
                coords=load_nii(fullfile(pn.voxeloverlap, ...
                    ['subcoords_nn_',IDs{indID},'_r', RunID{indRun},'.nii']));
                coords=reshape(coords.img,[],1);
                GM_coords_nn = cat(2, GM_coords_nn, coords);
            catch
                warning('File missing');
            end
        end
        disp (['Done with ' IDs{indID}])
    end

%% indices of non-NaN & non-zero GM voxels across subjects

    final_coords = find(nanmean(GM_coords_nn,2)==1);
    final_coords_nonzero = find(nanmean(cat(2, GM_coords_nn, GM_coords_nz),2)==1);

%% save coordinate mat

    save(fullfile(pn.voxeloverlap, ['coords_N',num2str(numel(IDs)),'.mat']), ...
        'nii_coords', 'final_coords', 'final_coords_nonzero');

%% save niftis
    
    tempNii = load_nii(fname_struct);

    nii_coords=zeros(64,76,64) ; % 3mm
    nii_coords(final_coords) = 1;
    tempNii.img = nii_coords;
    save_nii(tempNii,fullfile(pn.voxeloverlap, ...
        ['coords_noNaN_N',num2str(numel(IDs)),'.nii']))
    
    nii_coords=zeros(64,76,64) ; % 3mm
    nii_coords(final_coords_nonzero) = 1;
    tempNii.img = nii_coords;
    save_nii(tempNii,fullfile(pn.voxeloverlap, ...
        ['coords_nozero_N',num2str(numel(IDs)),'.nii']))
    
end
