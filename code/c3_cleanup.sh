#!/bin/bash

rootpath="$(pwd)/.."
rootpath=$(builtin cd $rootpath; pwd)

# delete log files
find ${rootpath}/data/voxeloverlap/ -name 'subcoords*' -delete

# detele intermediate coords files
find ${rootpath}/code/c_common_coords/logs/ -name '*.out' -delete