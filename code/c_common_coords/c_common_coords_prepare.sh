#!/bin/bash

# This script prepares tardis by compiling the necessary function in MATLAB.

# check [module avail matlab] and choose matlab version

fun_name="c_common_coords"

module load matlab/R2016b
matlab -nodesktop -nosplash -r "run ${fun_name}_prepare.m"
mv run_${fun_name}.sh ${fun_name}_run.sh
