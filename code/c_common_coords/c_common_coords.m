function c_common_coords(id, run, rootpath)

% - create 3d nifty of non-zero, non-NaN voxels

    if ismac % run if function is not pre-compiled
        id = '1126'; % test for example subject
        run = '1';
        currentFile = mfilename('fullpath');
        [pathstr,~,~] = fileparts(currentFile);
        cd(fullfile(pathstr,'..', '..'))
        rootpath = pwd;
    end

    pn.data_clean	= fullfile(rootpath, 'data', 'data_clean');
    pn.standards	= fullfile(rootpath, 'data', 'standards');
    pn.voxeloverlap = fullfile(rootpath, 'data', 'voxeloverlap');
    
    if ismac % run if function is not pre-compiled
        % NIFTI_toolbox
        addpath(genpath(fullfile(rootpath, 'tools', 'nifti_toolbox')));
    end

    disp(['Processing subject ', id, ' run ', run, '.']);
   
    %% Load 4D NIfTI file
        
    fname = fullfile(pn.data_clean, [id,'_run-',run,'_regressed.nii']);
    [img] = load_nii(fname);
    
    vox_non_zero = std(img.img,[],4)~=0;
    vox_non_nan = ~isnan(std(img.img,[],4));
    
    %% load 3d image and replace data in nifty structure
    
    fname_struct = fullfile(pn.standards, 'data', 'mni_icbm152_nlin_sym_09c', 'mni_icbm152_gm_tal_nlin_sym_09c_MNI_3mm.nii.gz');
    tempNii = load_nii(fname_struct);
    tempNii.img = vox_non_zero;
    save_nii(tempNii,fullfile(pn.voxeloverlap, ['subcoords_nz_',id,'_r', run,'.nii']))
    
    tempNii.img = vox_non_nan;
    save_nii(tempNii,fullfile(pn.voxeloverlap, ['subcoords_nn_',id,'_r', run,'.nii']))
       
    end